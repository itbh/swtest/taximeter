# Preisberechnung Taxameter

Als Taxiunternehmer möchte ich den Preis für Fahrstrecken berechnet bekommen,
um ihn auf dem Taxameter angezeigen zu können.

Anforderungskriterien:
* Die Methode liefert den Preis in Cent
* Jeder Kilometer kostet 1 EUR.
* Es werden nur ganze Kilometer berechnet.
* Auf Strecken mit 10km und mehr erhält der Kunde 5% Rabatt
* Auf Strecken mit 20km und mehr erhält der Kunde 10% Rabatt
* Rabatte werden immer auf die gesamte Strecke gerechnet. Es werden keine Teilstrecken berechnet
* Fahrten die nachts begonnen werden mit einem Aufschlag von 20% berechnet
  * Die Nacht startet um 22 Uhr. Der Tag startet um 06:00 Uhr
  * Das Ende der Fahrt ist irrelevant. Es zählt der Beginn der Fahrt. 
      Wer kurz vor 6:00 Uhr die Fahrt beginnt, zahlt also trotzdem für die gesamte Strecke Aufschlag.
* Der Preis wird in folgender Reihenfolge berechnet:
  1. Gepäckaufschlag
  2. Rabatt
  3. Aufschlag Nachtfahrt


Tip: Bei der Berechnung der Nachtfahrt werden nur Uhrzeiten betrachtet und nicht Tage. 
D.h. ich muss mir die Uhrzeit wie auf einem Zeitstrahl vorstellen:

|----------6--------------22---------|

Eine Uhrzeit kann nicht gleichzeitig vor 6 Uhr und nach 22 Uhr sein.
