package de.itbh.swtest;

import org.joda.time.LocalTime;

public class Taximeter implements ITaximeter {

	@Override
	public int calculate(int distance, LocalTime startTime, boolean luggage) {
		return -1;
	}

	@Override
	public boolean isNightride(LocalTime startTime) {
		return false;
	}

}
